<?php
/**
 * Конфиг
 */

$config = array();

/**
 * Настройки вывода блоков
 */
$config['$root$']['block']['rule_iframe_stream'] = array(
	'action'  => array(
			'streamiframe'
		),
	'blocks'  => array(
			'iframe_stream' => array(
				'stream'=>array('priority'=>0)
			)
		),
	'clear' => false,
);


$config['$root$']['router']['page']['streamiframe'] = 'PluginStreamiframe_ActionStreamiframe';

return $config;
