<?php

class PluginStreamiframe_ActionStreamiframe extends ActionPlugin {

    /**
     * Инициализация экшена
     */
    public function Init() {
        $this->SetDefaultEvent('index');
    }

    /**
     * Регистрируем евенты
     */
    protected function RegisterEvent() {
        $this->AddEvent('index','EventStreamiframe');
    }

    protected function EventStreamiframe() {
        $this->SetTemplateAction('index');
    }
}
?>
