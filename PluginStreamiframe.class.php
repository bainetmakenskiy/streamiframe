<?php

/**
 * Запрещаем напрямую через браузер обращение к этому файлу.
 */
if (!class_exists('Plugin')) {
    die('Hacking attemp!');
}

class PluginStreamiframe extends Plugin {

    // Активация плагина
    public function Activate() {

        return true;
    }

    // Деактивация плагина
    public function Deactivate(){

        return true;
    }


    // Инициализация плагина
    public function Init() {
        $this->Viewer_AppendStyle(Plugin::GetTemplatePath(__CLASS__)."css/streamiframe.css"); // Добавление своего CSS
    }
}
?>
